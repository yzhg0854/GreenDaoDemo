package com.yzhg.green;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.yzhg.green.bean.ClazzBean;
import com.yzhg.green.dao.operate.ClazzBeanOperate;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public String TAG = "============MainActivity=============";

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        /*创建数据集合*/

     /*   ClazzBean clazzBean = new ClazzBean();
        clazzBean.setClazzName("中华重点班班");
        clazzBean.setClazzCode("0000");

        //单个插入
        boolean insertClazzInfo = ClazzBeanOperate.getInstance().insertClazzInfo(clazzBean);
        if(insertClazzInfo){
            Log.i(TAG, "useAppContext: 插入成功");
        } else {
            Log.i(TAG, "useAppContext: 插入失败");
        }

        */



        /*批量插入数据*/
       /* List<ClazzBean> clazzBeanList = new ArrayList();
        for (int i = 0; i < 20; i++) {
            ClazzBean clazzBean1 = new ClazzBean();
            clazzBean1.setClazzName("中华" + (i + 20) + "班");
            clazzBean1.setClazzCode("00" + (i + 20));
            clazzBeanList.add(clazzBean1);
        }

        boolean intxClazz = ClazzBeanOperate.getInstance().insertIntxClazz(clazzBeanList);
        if (intxClazz) {
            Log.i(TAG, "useAppContext: 插入成功");
        } else {
            Log.i(TAG, "useAppContext: 插入失败");
        }
*/

       /* List<ClazzBean> clazzNameList = ClazzBeanOperate.getInstance().getClazzInfoList("中华重点班班");
        for (ClazzBean clazzBean : clazzNameList) {
            Log.i(TAG, "按名称查询: " + clazzBean.getClazzCode());
        }


        *//*修改第一条数据的code*//*
        ClazzBean clazzBean1 = clazzNameList.get(0);
        clazzBean1.setClazzCode("0000000000");
        boolean updateClazzInfo = ClazzBeanOperate.getInstance().updateClazzInfo(clazzBean1);
        if (updateClazzInfo) {
            Log.i(TAG, "updateClazzInfo: 更新成功");
        } else {
            Log.i(TAG, "updateClazzInfo: 更新失败");
        }


        List<ClazzBean> clazzInfoList = ClazzBeanOperate.getInstance().getClazzInfoList();
        for (ClazzBean clazzBean : clazzInfoList) {
            Log.i(TAG, "查询全部数据: "+clazzBean.getClazzName() +" == code == "+ clazzBean.getClazzCode());
        }*/

        List<ClazzBean> clazzBeanList = ClazzBeanOperate.getInstance().getClazzlimitInfo(5, 2, "中华");
        for (ClazzBean clazzBean : clazzBeanList) {
            Log.i(TAG, "查询全部数据: "+clazzBean.getClazzName() +" == code == "+ clazzBean.getClazzCode());
        }
    }
}


