package com.yzhg.green.bean;


import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity(nameInDb = "DB_CLAZZ")
public class ClazzBean {

    /*id  必须为long*/
    @Id(autoincrement = true)
    private Long _ID;


    //班级名称
    @Property(nameInDb = "CLAZZ_NAME")
    private String clazzName;

    //编辑编码
    @Property(nameInDb = "CLAZZ_CODE")
    private String clazzCode;

    @Generated(hash = 1489764607)
    public ClazzBean(Long _ID, String clazzName, String clazzCode) {
        this._ID = _ID;
        this.clazzName = clazzName;
        this.clazzCode = clazzCode;
    }

    @Generated(hash = 1492092479)
    public ClazzBean() {
    }

    public Long get_ID() {
        return this._ID;
    }

    public void set_ID(Long _ID) {
        this._ID = _ID;
    }

    public String getClazzName() {
        return this.clazzName;
    }

    public void setClazzName(String clazzName) {
        this.clazzName = clazzName;
    }

    public String getClazzCode() {
        return this.clazzCode;
    }

    public void setClazzCode(String clazzCode) {
        this.clazzCode = clazzCode;
    }

    @Override
    public String toString() {
        return "ClazzBean{" +
                "_ID=" + _ID +
                ", clazzName='" + clazzName + '\'' +
                ", clazzCode='" + clazzCode + '\'' +
                '}';
    }
}
