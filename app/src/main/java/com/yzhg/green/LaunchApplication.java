package com.yzhg.green;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.yzhg.green.dao.DBHelper;
import com.yzhg.green.dao.greendao.DaoMaster;
import com.yzhg.green.dao.greendao.DaoSession;

public class LaunchApplication extends Application {

    //数据库  写入
    private SQLiteDatabase sqLiteDatabase;
    private DaoSession daoSession;

    private static LaunchApplication application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        //初始化GreenDao
        initGreenDao();

    }

    public static LaunchApplication getApp() {
        return application;
    }

    /*
     * 初始化数据库操作
     * */
    private void initGreenDao() {
        //DevOpenHelper  name:  数据库名
        DBHelper mHelper = new DBHelper(this, "clazz_form.db", null);
        //获取到数据库写入
        sqLiteDatabase = mHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(sqLiteDatabase);
        //获取Session
        daoSession = daoMaster.newSession();
    }


    public DaoSession getDaoSession() {
        return daoSession;
    }

    public SQLiteDatabase getDb() {
        return sqLiteDatabase;
    }
}
















