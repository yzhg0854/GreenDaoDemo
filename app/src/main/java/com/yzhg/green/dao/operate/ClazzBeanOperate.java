package com.yzhg.green.dao.operate;


/*操作类*/

import android.util.Log;

import com.yzhg.green.LaunchApplication;
import com.yzhg.green.bean.ClazzBean;
import com.yzhg.green.dao.greendao.ClazzBeanDao;
import com.yzhg.green.dao.greendao.DaoSession;

import java.util.List;

public class ClazzBeanOperate {


    public String TAG = "============ClazzBeanOperate=============";

    private static ClazzBeanOperate instance;

    /*获取daoSession*/
    private final DaoSession daoSession;

    public static ClazzBeanOperate getInstance() {
        if (instance == null) {
            synchronized (ClazzBeanOperate.class) {
                if (instance == null) {
                    instance = new ClazzBeanOperate();
                }
            }
        }
        return instance;
    }


    public ClazzBeanOperate() {
        daoSession = LaunchApplication.getApp().getDaoSession();
    }


    /*增加用户信息*/
    public boolean insertClazzInfo(ClazzBean clazzBean) {
        try {
            long insert = daoSession.getClazzBeanDao().insert(clazzBean);
            return insert > 0;
        } catch (Exception e) {
            Log.i(TAG, "insertClazzInfo: " + e.getMessage());
            return false;
        }
    }

    /*批量插入数据*/
    public boolean insertIntxClazz(List<ClazzBean> clazzList) {
        if (clazzList == null || clazzList.isEmpty()) {
            return false;
        }
        try {
            daoSession.getClazzBeanDao().insertInTx(clazzList);
            ;
            return true;
        } catch (Exception e) {
            Log.i(TAG, "insertIntxClazz: " + e.getMessage());
            return false;
        }

    }

    /*删除表用的信息*/
    public boolean deleteClazzInfo() {
        try {
            daoSession.deleteAll(ClazzBean.class);
            return true;
        } catch (Exception e) {
            Log.i(TAG, "deleteClazzInfo: " + e.getMessage());
            return false;
        }
    }

    /*修改一条数据*/
    public boolean updateClazzInfo(ClazzBean clazzBean) {
        try {
            daoSession.update(clazzBean);
            return true;
        } catch (Exception e) {
            Log.i(TAG, "updateClazzInfo: " + e.getMessage());
            return false;
        }
    }

    /*查询所有数据*/
    public List<ClazzBean> getClazzInfoList() {

        return daoSession.getClazzBeanDao().queryBuilder().list();
    }


    /*根据班级名称查询一条数据*/
    public List<ClazzBean> getClazzInfoList(String clazzName) {
        return daoSession.getClazzBeanDao().queryBuilder().where(ClazzBeanDao.Properties.ClazzName.eq(clazzName)).list();
    }

    /*分页查询*/

    /**
     * 分页查询
     * @param limit  一页查询出多少数据
     * @param offset 页码
     * @param like  模糊查询
     * @return 返回查询出的数据
     */
    public List<ClazzBean> getClazzlimitInfo(int limit, int offset, String like) {
        return daoSession.getClazzBeanDao().queryBuilder().where(ClazzBeanDao.Properties.ClazzName.like("%" + like + "%")).limit(limit).offset(limit * offset).list();
    }
}
