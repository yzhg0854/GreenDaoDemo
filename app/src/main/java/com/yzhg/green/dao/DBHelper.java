package com.yzhg.green.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.yzhg.green.bean.ClazzBean;
import com.yzhg.green.dao.greendao.ClazzBeanDao;
import com.yzhg.green.dao.greendao.DaoMaster;

import org.greenrobot.greendao.database.Database;

public class DBHelper extends DaoMaster.OpenHelper {

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {

        MigrationHelper.migrate(db, new MigrationHelper.ReCreateAllTableListener() {

            @Override

            public void onCreateAllTables(Database db, boolean ifNotExists) {
                DaoMaster.createAllTables(db, ifNotExists);
            }

            @Override

            public void onDropAllTables(Database db, boolean ifExists) {
                DaoMaster.dropAllTables(db, ifExists);
            }
        }, ClazzBeanDao.class);

    }
}